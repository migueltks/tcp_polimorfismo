package com.ks.prueba;

import com.ks.tcp.Cliente;
import com.ks.tcp.Servidor;
import com.ks.tcp.Tcp;

/**
 * Hello world!
 *
 */
public class App 
{
    private static Tcp VMobjConexion;

    public static void main( String[] args )
    {
        int i = 2;

        if (i == 1)
        {
            VMobjConexion = new Servidor();
        }
        else
        {
            VMobjConexion = new Cliente();
            VMobjConexion.setIP("localhost");
        }
        VMobjConexion.setPuerto(2000);
        VMobjConexion.setEventos(new Comunicaciones());
        try
        {
            VMobjConexion.conectar();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
