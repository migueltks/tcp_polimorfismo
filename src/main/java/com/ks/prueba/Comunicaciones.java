package com.ks.prueba;

import com.ks.tcp.Cliente;
import com.ks.tcp.EventosTCP;
import com.ks.tcp.Servidor;
import com.ks.tcp.Tcp;

/**
 * Created by migue on 14/10/2016.
 */
public class Comunicaciones implements EventosTCP
{
    public Comunicaciones()
    {

    }

    public void conexionEstablecida(Cliente cliente)
    {
        cliente.enviar("Bienvenido");
    }

    public void errorConexion(String s)
    {

    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println("Mensaje: " + s);
    }

    public void cerrarConexion(Cliente cliente)
    {

    }
}
